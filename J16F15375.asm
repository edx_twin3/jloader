;**********************************************
;E. David Rojas Serrano <edx@edx-twin3.org>    *
;http://www.edx-twin3.org                      *
;Codigo JBoot                                  *
;**********************************************
 list p = PIC16F15375
 errorlevel -302
 errorlevel -307
 ;***************************************************************************
;Puedes cambiar los bits de configuracion para que se adapte a tu hardware *
;***************************************************************************
 __CONFIG 0x8007, 0x3FFC & 0x3F8F & 0x3FFF & 0x3FFF & 0x1FFF
 __CONFIG 0x8008, 0x3FFF & 0x3FFD & 0x3FFF & 0x3FBF & 0x3FFF & 0x3FFF & 0x37FF & 0x2FFF 
 __CONFIG 0x8009, 0x3FE0 & 0x3F9F & 0x3FFF & 0x3FFF
 __CONFIG 0x800A, 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x1FFF
 __CONFIG 0x800B, 0x3FFF
;***************************************************************
;Apartir de aqui modificalo solo si sabes lo que estas haciendo*
;***************************************************************
 org 0x00
 lgoto _JBoot
 org 0x02
_BdeProg goto $+0 ;Esperando un nuevo programa 
 
 org 0x1F00 ;Direccion 0x3F00 = 3840 (Direccion inicial)
_JBoot
;Configurando el reloj a 8MHZ
 movlb .17 ;Banco 17
 movlw 0x03
 movwf 0x893 ;OSCFRQ
 btfss 0x890,6 ;OSCSTAT (HFINTOSC = 1????)
 goto $-1
 ;Configurando puerto C
 movlb 0x00 ;Banco 0
 movlw 0x80 ;RC7 ->Rx,RC6 -> Tx (RC7,RC6 = 1 seg�n hoja de datos)
 movwf 0x14 ;TRISC
 movlb .62 ;Banco 62
 clrf 0x1F4E ;ANSELC
;**********************************************
;Configurando RC6 -> Tx (RxyPPS)
;Configurando RC7 -> Rx (xxxPPS) DEFAULT
;********************************************** 
 call _SecPPS
 bcf 0x1E8F,0 ;LOCK = Desbloqueado
 movlb .62 ;Banco 62
 movlw 0x0F ;Tx
 movwf 0x1F26 ;RC6PPS
 call _SecPPS
 bsf 0x1E8F,0 ;LOCK = Bloqueado
;Configurando TMR0
 movlb 0x0B ;Banco 11
 movlw 0x80 ;8 bits
 movwf 0x59E ;TMR0CON0
 movlw 0x4C ;Tiempo base 524.288ms
 movwf 0x59F ;TMR0CON1
;Configurando EUSART
 movlb 0x02 ;Banco 2
 movlw .12 ;9600
 movwf 0x11B ;SP1BRG1L
 bsf 0x11D,7 ;RC1STA1 (SPEN=1)
 bsf 0x11D,4 ;RC1STA1 (CREN=1)
 bsf 0x11E,5 ;TX1STA1 (TXEN=1)
;**********************************************
;Leyendo el modelo del microcontrolador
;********************************************** 
 movlb 0x10 ;Banco 16
 movlw 0x06 ;8006 <- Device ID
 movwf 0x81A ;NVMADRL
 clrf 0x81B  ;NVMADRH
 bsf 0x81E,6 ;NVMCON1 (Acceso Config/Device/ID)
 bsf 0x81E,0 ;NVMCON1 (Inicio de lectura)
 nop ;Primer ciclo que le toma para leer
 nop ;Segundo ciclo obligatorio
 ;**********************************************
;Guardando el modelo del microcontrolador
;**********************************************  
 movf 0x81C,0 ;NVMDATL
 movwf 0x70 ;ID (LSB)
 ;**********************************************
;Protocolo de comunicacion de JBoot
;<STX><0x55><0x4A>[<CMD>|<CHKSUM>]
;[CMD] 
;0x30 <---- Conexi�n establecida
;CHKSUM <---- Enviado a JLoader
;**********************************************
 movlb 0x02 ;Banco 2
 call _protoJB
 movlw 0x30 ;30 <---- Comando de identificacion
 call _EnvDato
 movlw 0xE4 ;<---- Versi�n JBoot (J16F182X)
 call _EnvDato 
 movlw 0x30 ;<---- ID micro (MSB)
 call _EnvDato
 movf 0x70,0 ;<---- ID micro (LSB)
 call _EnvDato
;**********************************************
;Seleccion de modo Boot/Normal
;**********************************************
 movlb 0x0B ;Banco 11
 clrf 0x59C ;TMR0
 movlb 0x0E ;Banco 14
 btfsc 0x70F,5 ;RCIF=0????
 goto _HostReply
 btfss 0x70C,5 ;TMR0IF=1????
 goto $-3
 bcf 0x70C,5 ;TMR0IF = 0
 goto _MNormal ;_BdeProg para dejar activo la EUSART
;**********************************************
;Codigo de verificacion JLoader
;**********************************************
_HostReply movlb 0x02 ;Banco 2
 movf 0x119,0 ;RC1REG1
 movwf 0x70 ;Registro temporal (RCREG)
 movlw 0x55 ;U Caracter recibido de JLoader 
 xorwf 0x70,0
 btfss 0x03,2
 goto _MNormal ;_BdeProg para dejar activo la EUSART 
;**********************************************
;Borrado memoria programa (32 words por fila)
;7936/32 = 248 => 248*32 = 7936
;**********************************************
 movlw .248
 movwf 0x70
 movlb 0x10 ;Banco 16
 clrf 0x81B ;NVMADRH ;Direccion de inicio(MSB)
 clrf 0x81A ;NVMADRL ;Direccion de inicio(LSB)
 bcf 0x81E,6 ;CFGS => Memoria Flash seleccionada
_BorBlock bsf 0x81E,4 ;Free => Habilita borrado FLASH
;Inicio de secuencia requerida 
 bsf 0x81E,2 ;WREN => Programacion habilitada
 call _secREQ
;Fin de la secuencia requerida
 bcf 0x81E,1 ;WR => Escritura deshabilitada
;Secuencia para borrar toda la memoria excepto el bootloader
 btfsc 0x81E,4 ;Free = 0
 goto $-1
 movlw 0x20 ;Numero de palabras (32)
 addwf 0x81A+0,1 ;Nueva fila de borrado
 btfss 0x03,2
 goto $+2
 incf 0x81A+1,1
 decfsz 0x70,1
 goto _BorBlock
;**********************************************
;Restableciendo las dos primeras palabras
;**********************************************
 clrf 0x81B ;NVMADRH ;Direccion de inicio(MSB)
 clrf 0x81A ;NVMADRL ;Direccion de inicio(LSB)
 bcf 0x81E,6 ;SET PFM
 bsf 0x81E,2 ;Habilita escritura
 bsf 0x81E,5 ;Carga los latches
 movlw 0x31 
 movwf 0x81D ;NVMDATH
 movlw 0x9F
 movwf 0x81C ;NVMDATL
 call _secREQ
 incf 0x81A,1
 movlw 0x2F
 movwf 0x81D ;NVMDATH
 movlw 0x00
 movwf 0x81C ;NVMDATL
 call _secREQ
 bcf 0x81E,5 ;Escribe los latches
 call _secREQ
 bcf 0x81E,2 ;Deshabilita escritura
;**********************************************
;Inicio de programacion FLASH
;0x70 ;Num datos
;0x71 ;Direccion y Datos
;****************************************************
_SecProg bsf 0x81E,2 ;WREN => Programacion habilitada
 bsf 0x81E,5 ;LWLO => Carga los latches
;****************************************************
;Datos
;**********************************************
 call _RecepDatos
 movwf 0x70 ;NumDatos
 addwf 0x09,1 ;Multiplicacion x 2 WREG (MODIFICADO)
 call _AddCHKSUM
;****************************************************
;Direcion
;********************************************** 
 call _RecepDatos
 movwf 0x81B ;NVMADRH
 movwf 0x05 ;FSR0H
 call _RecepDatos
 movwf 0x81A ;NVMADRL
 movwf 0x04 ;FSR0L
;Calculando sumatoria
 movf 0x05,0 ;Guardado en W
 addwf 0x05,1
 movf 0x04,0 ;Guardado en W
 addwf 0x04,1
 btfss 0x03,0 ;C = 1???? 
 goto $+2
 incf 0x04+1,1 ;FSRH1 (MSB)
 ;A�adiendo valores de direccion al chksum
 movf 0x05,0
 call _AddCHKSUM
 movf 0x04,0
 call _AddCHKSUM 
;****************************************************
;Datos
;****************************************************
_loopDRCN call _RecepDatos
 movwf 0x81D ;NVMDATH
 call _AddCHKSUM
 call _RecepDatos
 movwf 0x81C ;NVMDATL
 call _AddCHKSUM
 call _secREQ
 decfsz 0x70,1
 goto _IncDir
;**********************************************
;CHECKSUM --> FSR1 = MSB + LSB (16 bits)
;**********************************************
 movf 0x06,0 ;FSR1L (LSB)
 comf 0x09,1 ;Complemento 1
 addlw 0x01  ;Complemento 2
 movwf 0x70 ;registro CHKSUM
 clrf 0x04 ;LSB (ADDRES)
 clrf 0x05 ;MSB (ADDRES)
 clrf 0x06 ;LSB (CHKSUM)
 clrf 0x07 ;MSB (CHKSUM)
;**********************************************
;Enviando CHKSUM a JLoader para comparar 
;**********************************************
 movlb 0x02 ;Banco 2
 call _protoJB ;A�ADIDO 
 movf 0x70,0 ;CHKSUM ----> JLoader
 call _EnvDato
;**********************************************
;Recibiendo CHKSUM de JLoader para comparar 
;**********************************************
 call _RecepDatos
 xorwf 0x70,0
 btfss 0x03,2 ;***************** Bloque MODIFICADO ************
 goto _SecProg ;CHKSUM Incorrecto
 bcf 0x81E,5 ;LWLO => Escribe los latches
 call _secREQ
 bcf 0x81E,2 ;WREN => Programacion deshabilitada
 goto _SecProg ;CHKSUM Correcto
;**********************************************
_IncDir incf 0x81A+0,1
 btfss 0x03,2
 goto $+2
 incf 0x81A+1,1
 goto _loopDRCN 
_SecGrab bcf 0x195,5 ;LWLO => Escribe los latches
 call _secREQ
 bcf 0x195,2 ;WREN => Programacion deshabilitada
 goto _SecProg
;***********************************************
;Sumando los datos recividos
;***********************************************
_AddCHKSUM addwf 0x06,1  ;FSR1L  (LSB)
 btfss 0x03,0 ;C = 1???? 
 goto $+2
 incf 0x06+1,1 ;FSRH1 (MSB)
 return
;***********************************************
;PPS secuencia de desbloqueo
;***********************************************
_SecPPS movlb .61 ;Banco 61
 movlw 0x55
 movwf 0x1E8F ;PPSLOCK
 movlw 0xAA
 movwf 0x1E8F ;PPSLOCK
 return
;**********************************************
;Codigo de recepcion/verificacion de datos
;**********************************************
_RecepDatos movlb 0x0E ;Banco 14
 btfss 0x70F,5 ;RCIf =1????
 goto $-1
 movlb 0x02 ;Banco 2
 movf 0x119,0 ;RC1REG1
 movlb 0x10 ;Banco 16
 return
;***********************************************
;Secuencia requerida
;***********************************************
_secREQ movlw 0x55
 movwf 0x81F ;EECON2
 movlw 0xAA
 movwf 0x81F ;EECON2
 bsf 0x81E,1 ;EECON1,WR => Inicia la escritura
 return
;***********************************************
;Protocolo de com. de JBoot
;***********************************************
_protoJB movlw 0x02 ;STX
 call _EnvDato
 movlw 0x55 ;U
 call _EnvDato
 movlw 0x4A ;J
 call _EnvDato
 return
;**********************************************
;Enviar Datos (BANCO 2)
;**********************************************
_EnvDato movwf 0x11A ;TXREG1
 btfss 0x11E,1 ;TX1STA1 (TSR=1)
 goto $-1
 return 
;**********************************************
;Salida del bootloader
;**********************************************
_MNormal movlb 0x02 ;Banco 2
 bcf 0x11D,7 ;EUSART deshabilitada 
 movlb 0x00 ;Banco 0
 lgoto _BdeProg
;*********************************************************
;IMPORTANTE:
;Recuerda presionas el boton del MCLR para que 
;los registros esten como se muestran en la hoja de datos.
;*********************************************************
 end