;**********************************************
;E. David Rojas Serrano <edx@edx-twin3.org>    *
;http://www.edx-twin3.org                      *
;Codigo J12F1840                               *
;**********************************************
 list p = PIC12F1840
 errorlevel -302
;***************************************************************************
;Puedes cambiar los bits de configuracion para que se adapte a tu hardware *
;***************************************************************************
 __CONFIG 0x8007, 0x3FFC & 0x3FE7 & 0x3FDF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x2FFF & 0x1FFF
 __CONFIG 0x8008, 0x3FFF & 0x3EFF & 0x3DFF & 0x3FFF & 0x1FFF
 ;***************************************************************
;Apartir de aqui modificalo solo si sabes lo que estas haciendo*
;***************************************************************
 org 0x00
 lgoto _JBoot
 org 0x02 
_BdeProg goto $+0 ;Esperando un nuevo programa

 org 0xF00 ;Direccion de inicio
_JBoot
;Configurando el reloj a 8MHZ
 movlb 0x01 ;Banco 1
 movlw 0x70
 movwf 0x99 ;OSCCON
 btfss 0x9A,4 ;OSCSTAT
 goto $-1
 btfss 0x9A,3 ;OSCSTAT
 goto $-1
 btfss 0x9A,0 ;OSCSTAT
 goto $-1
;Configurando TMR0
 movlw 0x87 ;Tiempo base 32.768mseg
 movwf 0x95 ;OPTION_REG
;Configurando puerto (RA4 -> Tx,RA5 -> Rx)
 movlw 0x20
 movwf 0x8C ;TRISA
 movlb 0x02 ;Banco 2
 movlw 0x84
 movwf 0x11D ;APFCON
 movlb 0x03 ;Banco 3
 clrf 0x18C ;ANSELA
;Configurando EUSART
 movlw .12 ;9600
 movwf 0x19B;SPBRGL
 bsf 0x19D,7 ;RCSTA
 bsf 0x19D,4 ;RCSTA
 bsf 0x19E,5 ;TXSTA
;**********************************************
;Leyendo el modelo del microcontrolador
;********************************************** 
 movlw 0x06 ;8006 <- Device ID
 movwf 0x191 ;EEADRL
 clrf 0x192  ;EEADRH
 bsf 195,6 ;EECON1 (Acceso Config/Device/ID)
 bsf 195,0 ;EECON1 (Inicio de lectura)
 nop ;Primer ciclo que le toma para leer
 nop ;Segundo ciclo obligatorio
;**********************************************
;Guardando el modelo del microcontrolador
;**********************************************  
 movf 0x193,0 ;EEDATL LSB (0x100-----)
 andlw 0xE0 ;<---- Asegurando los 3 MSB (0x10000000)
 movwf 0x70 ;Registro temporal
 swapf 0x70,1 ;(0x00001000)
 rrf 0x70,1 ;(0x00000100)
 movf 0x194,0 ;EEDATH MSB (0x---11011)
 andlw 0x1F ;(0x00011011)
 rlf 0x09,1 
 rlf 0x09,1
 rlf 0x09,1 ;(0x11011000)
 addwf 0x70,1 ;(0x11011100)
 ;**********************************************
;Protocolo de comunicacion de JBoot
;<STX><0x55><0x4A>[<CMD>|<CHKSUM>]
;[CMD] 
;0x30 <---- Conexi�n establecida
;CHKSUM <---- Enviado a JLoader
;**********************************************
 call _protoJB
 movlw 0x30 ;30 <---- Comando de identificacion
 call _EnvDato
 movlw 0xE3 ;<---- Versi�n JBoot (J12F1840)
 call _EnvDato 
 movlw 0x00 ;<---- ID micro (MSB)
 call _EnvDato
 movf 0x70,0 ;<---- ID micro (LSB)
 call _EnvDato
;**********************************************
;Seleccion de modo Boot/Normal
;********************************************** 
 movlb 0x00 ;Banco 0
 movlw .15 ;Aproximadamente 0.5 seg
 movwf 0x70 ;Registro temporal contador
 clrf 0x15 ;TMR0
_TLoop btfsc 0x11,5 ;PIR1 (RCIF=0????) 
 goto _HostReply
 btfss 0x0B,2 ;INTCON(TMR0IF=1)
 goto $-3
 bcf 0x0B,2 ;TMR0IF = 0
 decfsz 0x70,1
 goto _TLoop
 goto _MNormal ;_BdeProg para dejar activo la EUSART
;**********************************************
;Codigo de verificacion y ajuste de transmision
;**********************************************
_HostReply movlb 0x03 ;Banco 3
 movf 0x199,0 ;RCREG
 movwf 0x70 ;Registro temporal (RCREG)
 movlw 0x55 ;U Caracter recibido de JLoader 
 xorwf 0x70,0
 btfss 0x03,2
 goto _MNormal ;_BdeProg para dejar activo la EUSART
;*****************************************************
;Borrado memoria programa (Borrado/Escritura 32 x 32)
;4096-256 = 3840
;3840/32 = 120 => 120*32 = 3840
;*****************************************************
 movlw 0x78
 movwf 0x70 ;Registro temporal contador
 clrf 0x192 ;Direccion de inicio alta (0x00)
 clrf 0x191 ;Direccion de inicio baja (0x00)
 bcf 0x195,6 ;CFGS => Memoria Flash seleccionada
 bsf 0x195,7 ;EEPGD => Apunta a la memoria flash
_BorBlock bsf 0x195,4 ;Free => Habilita borrado FLASH
 bsf 0x195,2 ;WREN => Programacion habilitada
;Inicio de secuencia requerida 
 call _secREQ
;Fin de la secuencia requerida
 bcf 0x195,2 ;WREN => Escritura deshabilitada
;Secuencia para borrar toda la memoria excepto el bootloader
 btfsc 0x0195,4 ;Free = 0
 goto $-1
 movlw 0x20 ;Numero de palabras (32)
 addwf 191+0,1 ;Nueva fila de borrado
 btfss 0x03,0
 goto $+2
 incf 0x191+1,1
 decfsz 0x70,1
 goto _BorBlock 
;**********************************************
;Restableciendo los apuntadores
;**********************************************
 bsf 0x195,2 ;WREN => Programacion habilitada
 bsf 0x195,5 ;LWLO => Carga los latches
 clrf 0x192 ;EEADRH
 clrf 0x191 ;EEADRL
 movlw 0x31
 movwf 0x194 ;EEDATH
 movlw 0x8F
 movwf 0x193 ;EEDATL
 call _secREQ
 incf 0x191,1 ;EEADRL
 movlw 0x2F
 movwf 0x194 ;EEDATH
 movlw 0x00
 movwf 0x193  ;EEDATL
 call _secREQ
 bcf 0x195,5 ;LWLO => Escribe los latches
 call _secREQ
 bcf 0x195,2 ;WREN => Programacion deshabilitada
;**********************************************
;Inicio de programacion FLASH
;0x70 ;NumDatos
;*****************************************************
_SecProg bsf 0x195,2 ;WREN => Programacion habilitada 
 bsf 0x195,5 ;LWLO => Carga los latches
;****************************************************
;Datos
;*****************************************************
 call _RecepDatos
 movwf 0x70 ;NumDatos
 addwf 0x09,1 ;Multiplicacion x 2 WREG (MODIFICADO)
 call _AddCHKSUM
 ;****************************************************
;Direcion
;********************************************** 
 call _RecepDatos
 movwf 0x192 ;EEADRH
 movwf 0x05 ;FSR0H
 call _RecepDatos
 movwf 0x191 ;EEADRL
 movwf 0x04 ;FSR0L
;Calculando sumatoria
 movf 0x05,0 ;Guardado en W
 addwf 0x05,1
 movf 0x04,0 ;Guardado en W
 addwf 0x04,1
 btfss 0x03,0 ;C = 1???? 
 goto $+2
 incf 0x04+1,1 ;FSRH1 (MSB)
 ;A�adiendo valores de direccion al chksum
 movf 0x05,0
 call _AddCHKSUM
 movf 0x04,0
 call _AddCHKSUM
;****************************************************
;Datos
;****************************************************
_loopDRCN call _RecepDatos
 movwf 0x194 ;EEDATH
 call _AddCHKSUM 
 call _RecepDatos
 movwf 0x193 ;EEDATL
 call _AddCHKSUM
 call _secREQ
 decfsz 0x70,1
 goto _IncDir
 ;**********************************************
;CHECKSUM --> FSR1 = MSB + LSB (16 bits)
;**********************************************
 movf 0x06,0 ;FSR1L (LSB)
 comf 0x09,1 ;Complemento 1
 addlw 0x01  ;Complemento 2
 movwf 0x70 ;registro CHKSUM
 clrf 0x04 ;LSB
 clrf 0x05 ;MSB
 clrf 0x06 ;LSB
 clrf 0x07 ;MSB
 ;**********************************************
;Enviando CHKSUM a JLoader para comparar 
;**********************************************
 call _protoJB ;A�ADIDO
 movf 0x70,0 ;CHKSUM ----> JLoader
 call _EnvDato
;**********************************************
;Recibiendo CHKSUM de JLoader para comparar 
;**********************************************
 call _RecepDatos
 xorwf 0x70,0
 btfss 0x03,2
 goto _SecProg ;CHKSUM incorrecto
 goto _SecGrab ;CHKSUM correcto
;***********************************************
;Codigo para incrementar la posicion de memoria 
;***********************************************
_IncDir incf 0x191+0,1
 btfss 0x03,2
 goto $+2
 incf 0x191+1,1
 goto _loopDRCN
;***********************************************
;Secuencia de grabacion
;*********************************************** 
_SecGrab bcf 0x195,5 ;LWLO => Escribe los latches
 call _secREQ
 bcf 0x195,2 ;WREN => Programacion deshabilitada
 goto _SecProg
;**********************************************
;Codigo de recepcion de datos
;**********************************************
_RecepDatos movlb 0x00 ;Banco 0
 btfss 0x11,5 ;RCIf =1????
 goto $-1
 movlb 0x03 ;Banco 3
 movf 0x199,0 ;RCREG
 return
;***********************************************
;Sumando los datos recividos
;***********************************************
_AddCHKSUM addwf 0x06,1  ;FSR1L  (LSB)
 btfss 0x03,0 ;C = 1???? 
 goto $+2
 incf 0x06+1,1 ;FSRH1 (MSB)
 return 
;***********************************************
;Secuencia requerida
;***********************************************
_secREQ movlw 0x55
 movwf 0x196 ;EECON2
 movlw 0xAA
 movwf 0x196 ;EECON2
 bsf 0x195,1 ;EECON1,WR => Inicia la escritura
 nop
 nop
 return 
 ;***********************************************
;Protocolo de com. de JBoot
;***********************************************
_protoJB movlw 0x02 ;STX
 call _EnvDato
 movlw 0x55 ;U
 call _EnvDato
 movlw 0x4A ;J
 call _EnvDato
 return
;**********************************************
;Enviar Datos Banco 3
;**********************************************
_EnvDato movwf 0x19A ;TXREG
 btfss 0x19E,1 ;TXSTA (TRMT)
 goto $-1
 return
 ;**********************************************
;Salida del bootloader
;**********************************************
_MNormal movlb 0x03 ;Banco 3
 bcf 0x19D,7 ;EUSART deshabilitada 
 movlb 0x00 ;Banco 0
 lgoto _BdeProg
;*********************************************************
;IMPORTANTE:
;Recuerda presionas el boton del MCLR para que 
;los registros esten como se muestran en la hoja de datos.
;*********************************************************
 end