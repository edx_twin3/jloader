;**********************************************
;E. David Rojas Serrano <edx@edx-twin3.org>    *
;http://www.edx-twin3.org                      *
;Codigo JBoot                                  *
;**********************************************
 List p=PIC16F1619
 errorlevel -302
;*******************************************************************************
;Bits de Configuraci�n
;*******************************************************************************
 __CONFIG 0x8007, 0x3FFC & 0x3FDF & 0x3FFF & 0x3FFF & 0x39FF & 0x3FFF & 0x2FFF & 0x1FFF
 __CONFIG 0x8008, 0x3FFF & 0x3FFB & 0x3FFF & 0x3EFF & 0x3DFF & 0x3FFF & 0x37FF & 0x1FFF
 __CONFIG 0x8009, 0x3FFF & 0x3F9F & 0x3FFF & 0x3FFF
;***************************************************************
;Apartir de aqui modificalo solo si sabes lo que estas haciendo*
;***************************************************************
 org 0x00
 lgoto _JBoot
 org 0x02
_BProg goto $+0

 org 0x1F00 ;0x1F00 = 7936 (Pagina inicial)
_JBoot
;Configurando el reloj a 8Mhz
 movlb 0x01 ;Banco 1
 movlw 0x6A
 movwf 0x99 ;OSCCON
 btfss 0x9A,4 ;OSCSTAT (HFIOFR)
 goto $-1
 btfss 0x9A,3 ;OSCSTAT (HFIOFL)
 goto $-1
 btfss 0x9A,0 ;OSCSTAT (HFIOFS)
 goto $-1
;Configurando Puerto B
 movlw 0x20 ;RB5 -> Rx, RB7 -> Tx Antes
 movwf 0x8D
;Configurando TMR0
 movlw 0x87 ;Tiempo base 32.768 mseg.
 movwf 0x95 ;OPTION_REG
;Configurando ANSELB
 movlb 0x03 ;Banco 3
 clrf 0x18D ;ANSELB
;Configurando EUSART
 movlw .25 ;9600
 movwf 0x19B ;SPBRGL
 bsf 0x19D,7 ;RC1STA (SPEN=1)
 bsf 0x19D,4 ;RC1STA (CREN=1)
 bsf 0x19E,5 ;TX1STA (TXEN=1)
 bsf 0x19E,2 ;TX1STA (BRGH=1)
;***********************************
;Configurando RxyPPS 
;RB7 -> Tx
;***********************************
 call _SecPPS
 bcf 0xE0F,0 ;LOCK = Desbloqueado
 ;Configurando RB5 -> Rx
 ;movlw 0x0D
 ;movwf 0xE24
 ;Configurando RB7 -> Tx
 movlb .29 ;Banco 29
 movlw 0x12
 movwf 0xE9F ;RB7PPS
 call _SecPPS
 bsf 0xE0F,1 ;LOCK = Bloqueado
;**********************************************
;Leyendo el modelo del microcontrolador
;********************************************** 
 movlb 0x03 ;Banco 3
 movlw 0x06 ;8006 <- Device ID
 movwf 0x191 ;PMADRL
 clrf 0x192 ;PMADRH
 bsf 0x195,6 ;PMCON1 (Acceso Config/Device/ID)
 bsf 0x195,0 ;Inicia la lectura
 nop
 nop
;**********************************************
;Guardando el modelo del microcontrolador
;**********************************************  
 movf 0x193,0 ;PMDATL
 movwf 0x1A0 ;Registro temproral
 ;**********************************************
;Protocolo de comunicacion de JBoot
;<STX><0x55><0x4A>[<CMD>|<CHKSUM>]
;[CMD] 
;0x30 <---- Conexi�n establecida
;CHKSUM <---- Enviado a JLoader
;**********************************************
 call _protoJB
 movlw 0x30
 call _EnvDato
 movlw 0xE1 ;<---- Versi�n JBoot (J16F161X)
 call _EnvDato
 movlw 0x30 ;<---- ID micro (MSB)
 call _EnvDato
 movf 0x1A0,0 ;<---- ID micro (LSB)
 call _EnvDato
;**********************************************
;Seleccion de modo Boot/Normal
;**********************************************
 movlb 0x00 ;Banco 0
 movlw .7 ;Aprox 0.5 segundo
 movwf 0x20 ;Registo temporal (timer)
 clrf 0x15 ;TMR0
_TLoop btfsc 0x10,5 ;RCIF=0????
 goto _HostReply
 btfss 0x0B,2 ;TMR0IF=1????
 goto $-3
 bcf 0x0B,2 ;TMR0IF = 0
 decfsz 0x20,1
 goto _TLoop
 goto _MNormal ;_BdeProg para dejar activo la EUSART
;**********************************************
;Codigo de verificacion y ajuste de transmision
;**********************************************
_HostReply movlb 0x03 ;Banco 3
 movf 0x199,0 ;RC1REG
 movwf 0x1A0 ;Registro temporal (RCREG)
 movlw 0x55 ;U Caracter recibido de JLoader
 xorwf 0x1A0,0
 btfss 0x03,2
 goto _MNormal ;_BdeProg para dejar activo la EUSART
;**********************************************
;Borrado memoria programa (32 words por fila)
;7936/32 = 248 => 248*32 = 7936
;**********************************************
 movlw .248
 movwf 0x1A0
 clrf 0x192 ;Direccion de inicio (MSB)
 clrf 0x191 ;Direccion de inicio (LSB)
 ;bsf 0x195,7 ;EEPGD => Acceso memoria programa
 bcf 0x195,6 ;CFGS => Memoria Flash seleccionada
_BorBlock bsf 0x195,4 ;Free => Habilita borrado FLASH
 bsf 0x195,2 ;WREN => Programacion habilitada
;Inicio de secuencia requerida
 call _secREQ
;Fin de la secuencia requerida
 bcf 0x195,1 ;WR => Escritura deshabilitada
;Secuencia para borrar toda la memoria excepto el bootloader
 btfsc 0x195,4 ;Free = 0
 goto $-1
 movlw 0x20 ;Numero de palabras (32)
 addwf 191+0,1 ;Nueva fila de borrado
 btfss 0x03,2
 goto $+2
 incf 0x191+1,1
 decfsz 0x1A0,1
 goto _BorBlock
;**********************************************
;Restableciendo las dos primeras palabras
;**********************************************
 bsf 0x195,2 ;WREN => Programacion habilitada
 bsf 0x195,5 ;LWLO => Carga los latches
 clrf 0x192 ;EEADRH
 clrf 0x191 ;EEADRL
 movlw 0x31
 movwf 0x194 ;EEDATH
 movlw 0x9F
 movwf 0x193 ;EEDATL
 call _secREQ
 incf 0x191,1 ;EEADRL
 movlw 0x2F
 movwf 0x194 ;EEDATH
 movlw 0x00
 movwf 0x193  ;EEDATL
 call _secREQ
 bcf 0x195,5 ;LWLO => Escribe los latches
 call _secREQ
 bcf 0x195,2 ;WREN => Programacion deshabilitada
;**********************************************
;Inicio de programacion FLASH
;0x1A0 ;Direccion/Datos
;0x1A1 ;NumDatos
;****************************************************
_SecProg bsf 0x195,2 ;WREN => Programacion habilitada
 bsf 0x195,5 ;LWLO => Carga los latches
;****************************************************
;Datos
;**************************************************** 
 call _RecepDatos
 movwf 0x1A1 ;NumDatos
 addwf 0x09,1 ;Multiplicacion 2 WREG (MODIFICADO)
 call _AddCHKSUM
;****************************************************
;Direccion
;****************************************************  
 call _RecepDatos
 movwf 0x192 ;EEADRH
 movwf 0x05 ;FSR0H
 call _RecepDatos
 movwf 0x191 ;EEADRL
 movwf 0x04 ;FSR0L
 ;Calculando la sumatoria
 movf 0x05,0 ;Guardado en W
 addwf 0x05,1
 movf 0x04,0 ;Guardado en W
 addwf 0x04,1
 btfss 0x03,0 ;C = 1???? 
 goto $+2
 incf 0x04+1,1 ;FSRH1 (MSB)
 ;A�adiendo valores de direccion al chksum
 movf 0x05,0
 call _AddCHKSUM
 movf 0x04,0
 call _AddCHKSUM
;****************************************************
;Datos
;***************************************************** 
_loopDRCN call _RecepDatos
 movwf 0x194 ;EEDATH
 call _AddCHKSUM
 call _RecepDatos
 movwf 0x193 ;EEDATL
 call _AddCHKSUM
 call _secREQ
 decfsz 0x1A1,1
 goto _IncDir
;**********************************************
;CHECKSUM --> FSR1 = MSB + LSB (16 bits)
;**********************************************
 movf 0x06,0 ;FSR1L (LSB)
 comf 0x09,1 ;Complemento 1
 addlw 0x01  ;Complemento 2
 movwf 0x1A2 ;registro CHKSUM
 clrf 0x04
 clrf 0x05
 clrf 0x06
 clrf 0x07
 ;**********************************************
;Enviando CHKSUM a JLoader para comparar 
;**********************************************
 call _protoJB ;A�ADIDO
 movf 0x1A2,0 ;CHKSUM ----> JLoader
 call _EnvDato
;**********************************************
;Recibiendo CHKSUM de JLoader para comparar 
;**********************************************
 call _RecepDatos
 xorwf 0x1A2,0
 btfss 0x03,2
 goto _SecProg ;CHKSUM incorrecto
 goto _SecGrab ;CHKSUM correcto
;**********************************************   
_IncDir incf 0x191+0,1
 btfss 0x03,2
 goto $+2
 incf 0x191+1,1
 goto _loopDRCN 
_SecGrab bcf 0x195,5 ;LWLO => Escribe los latches
 call _secREQ
 bcf 0x195,2 ;WREN => Programacion deshabilitada
 goto _SecProg
;**********************************************
;Codigo de recepcion/verificacion de datos
;**********************************************
_RecepDatos movlb 0x00 ;Banco 0
 btfss 0x10,5 ;RCIf =1????
 goto $-1
 movlb 0x03 ;Banco 3
 movf 0x199,0 ;RC1REG
 ;movwf 0x1A0 ;Registro Direccion/Datos
;**********************************************
;Codigo de salida EXISTE ALGUNO?????
;********************************************** 
 ;xorlw 0x58 ;X
 ;btfss 0x03,2
 ;goto $+2
 ;goto _MNormal
 ;movf 0x1A0,0
;**********************************************
;Reenviando dato recivido (Debug)
;**********************************************
 ;movwf 0x19A
 ;btfss 0x19E,1
 ;goto $-1 
 return
;***********************************************
;Sumando los datos recividos
;***********************************************
_AddCHKSUM  addwf 0x06,1  ;FSR1L  (LSB)
 btfss 0x03,0 ;C = 1???? 
 goto $+2
 incf 0x06+1,1 ;FSRH1 (MSB)
 return
;**********************************************
;Enviar Datos
;**********************************************
_EnvDato movwf 0x19A ;TX1REG
 btfss 0x19E,1
 goto $-1
 return
;***********************************************
;Protocolo de com. de JBoot
;***********************************************
_protoJB movlw 0x02 ;STX
 call _EnvDato
 movlw 0x55 ;U
 call _EnvDato
 movlw 0x4A ;J
 call _EnvDato
 return
;***********************************************
;Secuencia requerida
;***********************************************
_secREQ movlw 0x55
 movwf 0x196 ;EECON2
 movlw 0xAA
 movwf 0x196 ;EECON2
 bsf 0x195,1 ;EECON1,WR => Inicia la escritura
 nop
 nop
 return
;***********************************************
;Secuencia requerida PPS 
;***********************************************
_SecPPS movlb .28 ;Banco 28
 movlw 0x55
 movwf 0xE0F
 movlw 0xAA
 movwf 0xE0F
 return
;**********************************************
;Salida del bootloader
;**********************************************
_MNormal movlb 0x03 ;Banco 3
 bcf 0x19D,7 ;EUSART deshabilitada 
 movlb 0x00 ;Banco 0
 lgoto _BProg
;***********************************************  
 end