﻿## <b>JLoader source code</b>
En el 2017 termine la versión 1.0 de JLoader, desde entonces no le he echo ningún cambio, así de que en lugar de que se este desperdiciando este conocimiento místico :v, decidí lanzarlo al mundo, para que alguien mas lo pueda aprovechar (menos para hacer negocio, "eso no se hace apu :-@"), en fin, fue divertido crearlo, solo queda decir ..... vamos baby eres libre :'-3

# BURN & PLAY

La carpeta V1 contiene el código fuente de JLoader, fue creado utilizando Microsoft Visual Studio 2012,
Los otros archivos (*,asm) son el código fuente de los JBoots, según yo los trate de comentar lo mejor 

### <b>Hardware y Software</b>
Los JBoots fueron creados utilizando el que hasta ahora para mi ha sido el mejor IDE de programación de microcontroladores, y es el legendario [mplab V8.92](https://ww1.microchip.com/downloads/en/DeviceDoc/MPLAB_IDE_8_92.zip), por parte del hardware vas a necesitar un programador de micros, yo en su momento utilice el pickit 2, y para las versiones más nuevas fue necesario el pickit 3, la verdad el pickit 4 no lo utilice, pero fue mas porque para ese entonces ya no tenia tiempo de hacer nuevos JBoots, pero al igual y no esta de mas que lo tengan a la mano,
Claro esta, que pueden utilizar el código de JBoot como referencia (de echo tambien el de JLoader) para crear su bootloader para otras arquitecturas
> Nota: `Si lo quieren compartir con el mundo mandenlo para ponerlos juntos :1`

### <b>Autor</b>

Created by: [edx](mailto:edx@edx-twin3.org) 
<https://edx-twin3.org>

### <b>Licencia</b>

Copyright (c) 2022 edx-TwIn3.
All rights reserved.

This software is licensed under terms that can be found in the LICENSE file
in the root directory of this software component.
If no LICENSE file comes with this software, it is provided AS-IS.