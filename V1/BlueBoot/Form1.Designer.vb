﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JLoader
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JLoader))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ArDir = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Consola = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.BProgram = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Puertos = New System.Windows.Forms.ComboBox()
        Me.BConectar = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Cabecera = New System.Windows.Forms.Panel()
        Me.minimiza = New System.Windows.Forms.Button()
        Me.Title = New System.Windows.Forms.Label()
        Me.cerrrar = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.Cabecera.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Location = New System.Drawing.Point(370, 16)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(67, 30)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "C&argar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ArDir
        '
        Me.ArDir.AutoSize = True
        Me.ArDir.Location = New System.Drawing.Point(11, 25)
        Me.ArDir.Name = "ArDir"
        Me.ArDir.Size = New System.Drawing.Size(79, 13)
        Me.ArDir.TabIndex = 1
        Me.ArDir.Text = "Ningun archivo"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ArDir)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(6, 51)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(451, 55)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Seleccion de archivo"
        '
        'Consola
        '
        Me.Consola.AcceptsReturn = True
        Me.Consola.AcceptsTab = True
        Me.Consola.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Consola.Location = New System.Drawing.Point(6, 18)
        Me.Consola.Multiline = True
        Me.Consola.Name = "Consola"
        Me.Consola.ReadOnly = True
        Me.Consola.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Consola.Size = New System.Drawing.Size(240, 175)
        Me.Consola.TabIndex = 3
        Me.Consola.Text = "JLoader: Version 1.0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "edx <edx@edx-twin3.org>" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "https://www.edx-twin3.org/JLoader." & _
    "html" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Instrucciones:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "> Seleccione un puerto COM" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "> Presione el botón ""Conectar""" & _
    " (Alt+C)"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Consola)
        Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(205, 112)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(252, 199)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Consola"
        '
        'BProgram
        '
        Me.BProgram.Enabled = False
        Me.BProgram.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BProgram.Location = New System.Drawing.Point(21, 18)
        Me.BProgram.Name = "BProgram"
        Me.BProgram.Size = New System.Drawing.Size(111, 30)
        Me.BProgram.TabIndex = 5
        Me.BProgram.Text = "&Programar"
        Me.BProgram.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.BProgram)
        Me.GroupBox3.Location = New System.Drawing.Point(9, 229)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(166, 61)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Iniciar secuencia de grabacion"
        '
        'Puertos
        '
        Me.Puertos.FormattingEnabled = True
        Me.Puertos.Location = New System.Drawing.Point(4, 17)
        Me.Puertos.Name = "Puertos"
        Me.Puertos.Size = New System.Drawing.Size(155, 21)
        Me.Puertos.TabIndex = 7
        '
        'BConectar
        '
        Me.BConectar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BConectar.Location = New System.Drawing.Point(41, 44)
        Me.BConectar.Name = "BConectar"
        Me.BConectar.Size = New System.Drawing.Size(84, 30)
        Me.BConectar.TabIndex = 8
        Me.BConectar.Text = "&Conectar"
        Me.BConectar.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.BConectar)
        Me.GroupBox4.Controls.Add(Me.Puertos)
        Me.GroupBox4.Location = New System.Drawing.Point(6, 113)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(169, 81)
        Me.GroupBox4.TabIndex = 9
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Puerto"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.ActiveLinkColor = System.Drawing.SystemColors.MenuHighlight
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.LinkLabel1.LinkColor = System.Drawing.SystemColors.Highlight
        Me.LinkLabel1.Location = New System.Drawing.Point(7, 299)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(146, 15)
        Me.LinkLabel1.TabIndex = 10
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "https://www.edx-twin3.org"
        '
        'SerialPort1
        '
        Me.SerialPort1.PortName = "COM8"
        '
        'Timer1
        '
        Me.Timer1.Interval = 3000
        '
        'Cabecera
        '
        Me.Cabecera.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Cabecera.Controls.Add(Me.minimiza)
        Me.Cabecera.Controls.Add(Me.Title)
        Me.Cabecera.Controls.Add(Me.cerrrar)
        Me.Cabecera.Dock = System.Windows.Forms.DockStyle.Top
        Me.Cabecera.Location = New System.Drawing.Point(0, 0)
        Me.Cabecera.Name = "Cabecera"
        Me.Cabecera.Size = New System.Drawing.Size(469, 45)
        Me.Cabecera.TabIndex = 11
        '
        'minimiza
        '
        Me.minimiza.BackColor = System.Drawing.Color.Transparent
        Me.minimiza.FlatAppearance.BorderSize = 0
        Me.minimiza.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.minimiza.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.minimiza.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.minimiza.Location = New System.Drawing.Point(303, 19)
        Me.minimiza.Name = "minimiza"
        Me.minimiza.Size = New System.Drawing.Size(91, 30)
        Me.minimiza.TabIndex = 2
        Me.minimiza.Text = "Minimizar"
        Me.minimiza.UseVisualStyleBackColor = False
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Times New Roman", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Title.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Title.Location = New System.Drawing.Point(5, 2)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(152, 42)
        Me.Title.TabIndex = 1
        Me.Title.Text = "JLoader"
        '
        'cerrrar
        '
        Me.cerrrar.BackColor = System.Drawing.Color.Transparent
        Me.cerrrar.FlatAppearance.BorderSize = 0
        Me.cerrrar.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control
        Me.cerrrar.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control
        Me.cerrrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cerrrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cerrrar.ForeColor = System.Drawing.SystemColors.Highlight
        Me.cerrrar.Location = New System.Drawing.Point(397, 20)
        Me.cerrrar.Name = "cerrrar"
        Me.cerrrar.Size = New System.Drawing.Size(69, 29)
        Me.cerrrar.TabIndex = 0
        Me.cerrrar.Text = "Cerrar"
        Me.cerrrar.UseVisualStyleBackColor = False
        '
        'JLoader
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(469, 323)
        Me.Controls.Add(Me.Cabecera)
        Me.Controls.Add(Me.LinkLabel1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(469, 369)
        Me.Name = "JLoader"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "JLoader"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.Cabecera.ResumeLayout(False)
        Me.Cabecera.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ArDir As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Consola As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents BProgram As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Puertos As System.Windows.Forms.ComboBox
    Friend WithEvents BConectar As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Cabecera As System.Windows.Forms.Panel
    Friend WithEvents cerrrar As System.Windows.Forms.Button
    Friend WithEvents Title As System.Windows.Forms.Label
    Friend WithEvents minimiza As System.Windows.Forms.Button

End Class
