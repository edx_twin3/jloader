﻿Imports System.IO
Imports System.Text
Imports System.IO.Ports
Imports System.Configuration
Public Class JLoader
    Dim mouse_move As System.Drawing.Point
    Dim flag As Boolean
    Dim RTIF As Boolean
    Dim StrBufferOut As String
    Dim timeout As Byte
    Dim intent As Byte = 4
    Dim Adress As UShort
    Dim resultado, A1, A2, A3, A4, type As Byte
    Dim memmax As UInteger
    Dim NumLines As UInteger = 0
    Dim TempL As String
    Dim FileNum As Integer = FreeFile()
    Private StrBufferIn As Byte()
    Private Delegate Sub UpdateFormDelegate()
    Private UpdateFormDelegate1 As UpdateFormDelegate
    Public Sub ArchivoHex(ByVal index As Byte)
        Dim Valor() As Char = TempL.ToCharArray
        resultado = Asc(Valor(index))
        If resultado = 65 Then
            resultado = Convert.ToByte(10)
        ElseIf resultado = 66 Then
            resultado = Convert.ToByte(11)
        ElseIf resultado = 67 Then
            resultado = Convert.ToByte(12)
        ElseIf resultado = 68 Then
            resultado = Convert.ToByte(13)
        ElseIf resultado = 69 Then
            resultado = Convert.ToByte(14)
        ElseIf resultado = 70 Then
            resultado = Convert.ToByte(15)
        Else
            resultado = Val(Valor(index))
        End If
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        OpenFileDialog1.Title = "JLoader"
        OpenFileDialog1.Filter = "Archivo (*.hex)|*.hex"
        OpenFileDialog1.FileName = ""
        OpenFileDialog1.ShowDialog()
        ArDir.Text = System.IO.Path.GetFileName(OpenFileDialog1.FileName)
        If OpenFileDialog1.FileName = "" Then
            ArDir.Text = "Ningun archivo"
            Consola.Text = "Carga abortada"
            BProgram.Enabled = False
            GoTo verr
        ElseIf New FileInfo(OpenFileDialog1.FileName).Length = 0 Then
            Consola.Text = "Error: Archivo vacio"
            ArDir.Text = "Ningun archivo"
            BProgram.Enabled = False
            GoTo verr
        Else
            Consola.Text = "Archivo cargado"
        End If
        Consola.Text = Consola.Text & vbCrLf & "Verificando archivo ...."
        'Verificando si es del tipo = 00
        FileOpen(FileNum, OpenFileDialog1.FileName, OpenMode.Input)
        Try
            Do Until EOF(FileNum)
                TempL = LineInput(FileNum)
                TempL = TempL.TrimStart(":") 'Eliminando los dos puntos de inicio
                ArchivoHex(6)
                type = resultado
                ArchivoHex(7)
                type = type + resultado
                If type = 0 Then
                    ArchivoHex(2)
                    A1 = resultado
                    ArchivoHex(3)
                    A2 = resultado
                    ArchivoHex(4)
                    A3 = resultado
                    ArchivoHex(5)
                    A4 = resultado
                    Adress = A1
                    Adress = Adress << 4
                    Adress = Adress + A2
                    Adress = Adress << 4
                    Adress = Adress + A3
                    Adress = Adress << 4
                    Adress = Adress + A4
                    Adress = Adress / 2 'Direccion de inicio
                    NumLines = NumLines + 1
                    If Adress = 0 Or Adress = 1 Then
                        Consola.Text = Consola.Text & vbCrLf & "Error: Conflicto con apuntador"
                        Consola.Text = Consola.Text & vbCrLf & ""
                        Consola.Text = Consola.Text & vbCrLf & "Solución:"
                        Consola.Text = Consola.Text & vbCrLf & "*Inicie el programa en 0x02"
                        GoTo verr
                    ElseIf Adress >= memmax Then
                        Consola.Text = Consola.Text & vbCrLf & "Error: Conflicto con JBoot"
                        Consola.Text = Consola.Text & vbCrLf & ""
                        Consola.Text = Consola.Text & vbCrLf & "Solución:"
                        Consola.Text = Consola.Text & vbCrLf & "*No sobrepase la dirección: " & Conversion.Hex(memmax)
                        GoTo verr
                    End If
                End If
            Loop
        Catch ex As Exception
        End Try
        Consola.Text = Consola.Text & vbCrLf & "No se detectaron errores"
        Consola.Text = Consola.Text & vbCrLf & "> Presione Programar (Alt+P)"
        BProgram.Enabled = True
verr:
        FileClose(FileNum)
    End Sub

    Private Sub BProgram_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BProgram.Click
        SerialPort1.DiscardNull = False 'ESTE ES EL QUE TE DECIA ABAJITO
        SerialPort1.DiscardInBuffer() 'Añadido
        intent = 1
        Dim Comando, CHKSM As UShort
        Dim LCHKSUM, NumDatos, TCHKSM, MSB, LSB, C1, C2, C3, C4, i As Byte
        Consola.Text = "Programando ...."
        BProgram.Text = "Programando ...."
        BProgram.Enabled = False
        Button1.Enabled = False
        'Leyendo archivo linea por linea
        FileOpen(FileNum, OpenFileDialog1.FileName, OpenMode.Input)
        Try
            Do Until EOF(FileNum)
                TempL = LineInput(FileNum)
                TempL = TempL.TrimStart(":") 'Eliminando los dos puntos de inicio
Err:
                'Byte numero de datos"
                ArchivoHex(0)
                MSB = resultado
                ArchivoHex(1)
                LSB = resultado
                'Vaciando datos en el registro
                NumDatos = MSB
                NumDatos = NumDatos << 4
                NumDatos = NumDatos + LSB
                CHKSM = NumDatos 'Valor añadido al Checksum
                NumDatos = NumDatos / 2 'Numero de nemonicos a ser enviados
                '2 Bytes de direccion
                ArchivoHex(2)
                A1 = resultado
                ArchivoHex(3)
                A2 = resultado
                ArchivoHex(4)
                A3 = resultado
                ArchivoHex(5)
                A4 = resultado
                'Añadiendo al checksum
                TCHKSM = A1
                TCHKSM = TCHKSM << 4
                TCHKSM = TCHKSM + A2
                CHKSM = CHKSM + TCHKSM 'Añadiendo valores a la suma de comprobacion
                TCHKSM = A3
                TCHKSM = TCHKSM << 4
                TCHKSM = TCHKSM + A4
                CHKSM = CHKSM + TCHKSM 'Añadiendo valores a la suma de comprobacion
                'Vaciando datos en el registro
                Adress = A1
                Adress = Adress << 4
                Adress = Adress + A2
                Adress = Adress << 4
                Adress = Adress + A3
                Adress = Adress << 4
                Adress = Adress + A4
                Adress = Adress / 2 'Direccion de inicio
                'Guardando Checksum leido 
                i = (NumDatos * 4) + 8 'Checksum del archivo leido (LCHKSUM)
                ArchivoHex(i)
                LCHKSUM = resultado
                LCHKSUM = LCHKSUM << 4
                i = i + 1
                ArchivoHex(i)
                LCHKSUM = LCHKSUM + resultado
                '************************************************************************************************************************************
                'Tipo de datos
                '************************************************************************************************************************************
                'Byte de tipo
                ArchivoHex(6)
                type = resultado
                ArchivoHex(7)
                type = type + resultado
                CHKSM = CHKSM + type 'Valor añadido al Checksum
                If type = 0 Then
                    'Tipo: Datos
                    i = 8
                    '****************************************************************************************************************************
                    'Enviando datos al microcontrolador
                    '****************************************************************************************************************************
                    'Numero de datos
                    Dim D0() As Byte = BitConverter.GetBytes(NumDatos)
                    SerialPort1.Write(D0, 0, 1) 'Enviando numero de datos
                    'Direccion
                    Dim TAdress() As Byte = BitConverter.GetBytes(Adress)
                    A1 = TAdress(1)
                    Dim D1() As Byte = BitConverter.GetBytes(A1)
                    SerialPort1.Write(D1, 0, 1) 'Enviando direccion
                    A2 = TAdress(0)
                    Dim D2() As Byte = BitConverter.GetBytes(A2)
                    SerialPort1.Write(D2, 0, 1)
                    For aux As Byte = 1 To NumDatos
iter:
                        ArchivoHex(i)
                        If i = 8 OrElse i = 12 OrElse i = 16 OrElse i = 20 OrElse i = 24 OrElse i = 28 OrElse i = 32 OrElse i = 36 Then
                            C1 = resultado
                            i = i + 1
                            GoTo iter
                        ElseIf i = 9 OrElse i = 13 OrElse i = 17 OrElse i = 21 OrElse i = 25 OrElse i = 29 OrElse i = 33 OrElse i = 37 Then
                            C2 = resultado
                            i = i + 1
                            GoTo iter
                        ElseIf i = 10 OrElse i = 14 OrElse i = 18 OrElse i = 22 OrElse i = 26 OrElse i = 30 OrElse i = 34 OrElse i = 38 Then
                            C3 = resultado
                            i = i + 1
                            GoTo iter
                        ElseIf i = 11 OrElse i = 15 OrElse i = 19 OrElse i = 23 OrElse i = 27 OrElse i = 31 OrElse i = 35 OrElse i = 39 Then
                            C4 = resultado
                            i = i + 1
                        End If
                        'Añadiendo al checksum
                        TCHKSM = C1
                        TCHKSM = TCHKSM << 4
                        TCHKSM = TCHKSM + C2
                        CHKSM = CHKSM + TCHKSM 'Añadiendo valores a la suma de comprobacion
                        TCHKSM = C3
                        TCHKSM = TCHKSM << 4
                        TCHKSM = TCHKSM + C4
                        CHKSM = CHKSM + TCHKSM 'Añadiendo valores a la suma de comprobacion
                        'Reacomodando los valores
                        Comando = C3
                        Comando = Comando << 4
                        Comando = Comando + C4
                        Comando = Comando << 4
                        Comando = Comando + C1
                        Comando = Comando << 4
                        Comando = Comando + C2
                        'Separando en Bytes (2 Bytes)
                        Dim TComando() As Byte = BitConverter.GetBytes(Comando)
                        'Enviando Nemonico
                        MSB = TComando(1)
                        Dim D3() As Byte = BitConverter.GetBytes(MSB) 'Cargando el nemonico
                        SerialPort1.Write(D3, 0, 1) 'Enviando direccion
                        LSB = TComando(0)
                        Dim D4() As Byte = BitConverter.GetBytes(LSB) 'Cargando el nemonico
                        SerialPort1.Write(D4, 0, 1) 'Enviando direccion
                        'Consola.Text = Consola.Text & vbCrLf & "Direccion: " & Adress & " Nemonico: " & Conversion.Hex(Comando)
                        'Adress = Adress + 1
                    Next
                    'System.Threading.Thread.Sleep(2) 'Respiro al main thread, mmmmmmm al parecer sale peor con este culero 
                    RTIF = False
                    Do Until RTIF
                    Loop
                    'Comprobando los datos Leidos y enviados
                    CHKSM = CHKSM Mod 256
                    CHKSM = CHKSM Xor &HFF 'Complemento a 1
                    CHKSM = CHKSM + 1 'Complemento a 2
                    Dim D5() As Byte = BitConverter.GetBytes(CHKSM)
                    CHKSM = D5(0)
                    SerialPort1.Write(D5, 0, 1) 'Enviando CHKSUM calculado
                    'MessageBox.Show(StrBufferIn(3), "Recibido", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If CHKSM = LCHKSUM And Val(StrBufferIn(3)) = LCHKSUM Then
                        Consola.Text = "Checksum leido: " & Conversion.Hex(LCHKSUM) & vbCrLf & "Checksum enviado: " & Conversion.Hex(CHKSM) & vbCrLf & "Checksum recibido: " & Conversion.Hex(Val(StrBufferIn(3)))
                    Else
                        Consola.Text = "Error detectado" & vbCrLf & "Volviendo a enviar datos"
                        intent = intent - 1
                        If intent > 0 Then
                            GoTo Err
                        Else
                            Consola.Text = "Limite de errores alcanzado!!!!"
                            Consola.Text = Consola.Text & vbCrLf & "Direccion: " & Adress & " Nemonico: " & Conversion.Hex(Comando)
                            Consola.Text = Consola.Text & vbCrLf & "Checksum leido: " & Conversion.Hex(LCHKSUM) & vbCrLf & "Checksum enviado: " & Conversion.Hex(CHKSM) & vbCrLf & "Checksum recibido: " & Conversion.Hex(Val(StrBufferIn(3)))
                            BProgram.Text = "Programar"
                            ArDir.Text = "Ningun archivo"
                            BConectar.Enabled = True
                            Puertos.Enabled = True
                            Consola.Text = Consola.Text & vbCrLf & "Cerrando puerto: " & Puertos.Text
                            SerialPort1.Close()
                            Consola.Text = Consola.Text & vbCrLf & ""
                            Consola.Text = Consola.Text & vbCrLf & "Posibles causas: " & vbCrLf & "1.- Archivo corrupto" & vbCrLf & "2.- Comunicación con JBoot"
                            Consola.Text = Consola.Text & vbCrLf & ""
                            Consola.Text = Consola.Text & vbCrLf & "Posibles soluciones:" & vbCrLf & "*Vuelva a generar el archivo HEX" & vbCrLf & "*Reinicie la comunicación con JBoot"
                            Consola.Text = Consola.Text & vbCrLf & ""
                            Consola.Text = Consola.Text & vbCrLf & "NOTA: Si esta utilizando un módulo de RF por ejemplo HC05 aleje un poco el modulo."
                            GoTo fatal
                        End If
                    End If
                ElseIf type = 1 Then
                    'Tipo: EOF
                    Consola.Text = Consola.Text & vbCrLf & "Tipo: EOF"
                    'StrBufferOut = Conversion.Hex(Asc("X")) 'Codigo de salida
                    'SerialPort1.Write(StrBufferOut) 'Enviando nemonico
                ElseIf type = 4 Then
                    'Tipo: Direccion Extendida 
                    Consola.Text = Consola.Text & vbCrLf & "Tipo: Dirección extendida"
                Else
                    Consola.Text = "Tipo: Desconocido"
                End If
            Loop
        Catch ex As Exception
        End Try
        BProgram.Text = "Programar"
        ArDir.Text = "Ningun archivo"
        BConectar.Enabled = True
        Puertos.Enabled = True
        Consola.Text = Consola.Text & vbCrLf & "Cerrando puerto: " & Puertos.Text
        SerialPort1.Close()
        Consola.Text = Consola.Text & vbCrLf & "Programacion finalizada correctamente"
        Consola.Text = Consola.Text & vbCrLf & "> Reinicie el microcontrolador (MCLR)"
fatal:
        FileClose(FileNum)
    End Sub

    Private Sub JLoader_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        AddHandler SerialPort1.DataReceived, AddressOf SerialPort1_DataReceived
        Puertos.Items.Clear()
        StrBufferOut = ""
        For Each Modulo As String In My.Computer.Ports.SerialPortNames
            Puertos.Items.Add(Modulo)
        Next
        If Puertos.Items.Count > 0 Then
            Puertos.Text = Puertos.Items(0)
            BConectar.Enabled = True
        Else
            MessageBox.Show("No se ha encontrado ningun puerto de comunicacion", "JLoader", MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning)
            BConectar.Enabled = False
            Close()
        End If
        Puertos.Select()
        Button1.Enabled = False
    End Sub
    Private Sub BConectar_Click(sender As Object, e As EventArgs) Handles BConectar.Click
        flag = False
        intent = 4

        Try
            With SerialPort1
                .BaudRate = 9600
                .DataBits = 8
                .Parity = Parity.None
                .StopBits = StopBits.One
                .PortName = Puertos.Text
                .Handshake = Handshake.None
                .DiscardNull = True 'Deshabilita ESTO EN EL AREA DEL BOOTLOADER
                .ReadTimeout = 500
                .Open()
            End With

        Catch ex As Exception
            MessageBox.Show("Error de comunicacion", "JLoader", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Consola.Text = "Causa: " & vbCrLf & "1.- Puerto de comunicación invalido"
            Consola.Text = Consola.Text & vbCrLf & ""
            Consola.Text = Consola.Text & vbCrLf & "Solución:" & vbCrLf & "*Seleccione otro puerto de comunicación"
        End Try

        If SerialPort1.IsOpen Then
            timeout = 30
            Timer1.Start()
            Consola.Text = "Conectado al puerto: " & Puertos.Text
            Consola.Text = Consola.Text & vbCrLf & "Estableciendo conexión con JBoot ...."
            Consola.Text = Consola.Text & vbCrLf & "> Presione y SUELTE el MCRL ...."
            Consola.Text = Consola.Text & vbCrLf & "> Tiempo de espera: 3 seg"
        End If
    End Sub
    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Process.Start("https://www.edx-twin3.org")
    End Sub

    Private Sub Comando(P As Char)
        'Estableciendo comunicacion con el microcontrolador
        'Protocolo JLoader: <U(85)><J(74)><CMD>
        'I ---> Modelo del micro
        'R ---> Leer memoria del micro
        'C ---> Checksum
        'K ---> Ok
        'E ---> Error
        StrBufferOut = "U"
        SerialPort1.Write(StrBufferOut, 0, 1) 'Enviando dato
        StrBufferOut = "J"
        SerialPort1.Write(StrBufferOut, 0, 1) 'Enviando dato
        SerialPort1.Write({P}, 0, 1) 'Enviando dato
    End Sub

    Private Sub PicCom()
        Timer1.Stop()
        Dim valor() As String
        '*******************************************************************************************************************************************
        'Comunicacion con el microcontrolador
        'Protocolo micro: <STX(2)><U(85)><J(74)><CMD>[Data...]
        '*******************************************************************************************************************************************
        Try
            If flag = False Then
                'If StrBufferIn(0) = 2 And StrBufferIn(1) = 85 And StrBufferIn(2) = 74 Then
                If StrBufferIn(1) = 85 And StrBufferIn(2) = 74 And StrBufferIn(3) = 48 Then
                    Dim JVer As Byte = StrBufferIn(4) 'Version JBoot
                    Dim ID As UShort = StrBufferIn(5) 'ID
                    ID = ID << 8
                    ID = ID + StrBufferIn(6)
                    StrBufferOut = "U"
                    SerialPort1.Write(StrBufferOut, 0, 1) 'Enviando dato
                    Consola.Text = "Autenticación: Correcta"
                    Consola.Text = Consola.Text & vbCrLf & "Conexión establecida con JBoot"
                    Consola.Text = Consola.Text & vbCrLf & "Recibiendo informacion ...."
                    valor = ConfigurationManager.AppSettings(Convert.ToString(JVer)).Split(",")
                    Consola.Text = Consola.Text & vbCrLf & "Versión JBoot: " & valor(0)
                    If IsNothing(ConfigurationManager.AppSettings(Convert.ToString(ID))) Then
                        Consola.Text = Consola.Text & vbCrLf & "Microcontrolador: ????"
                        memmax = 255
                    Else
                        valor = ConfigurationManager.AppSettings(Convert.ToString(ID)).Split(",")
                        Consola.Text = Consola.Text & vbCrLf & "Microcontrolador: " & valor(0)
                        memmax = valor(1)
                    End If
                    'Consola.Text = Consola.Text & vbCrLf & "Memoria max: " & memmax
                    Consola.Text = Consola.Text & vbCrLf & "> Cargue el archivo hex (Alt+A)"
                    Puertos.Enabled = False
                    BConectar.Enabled = False
                    Button1.Enabled = True
                    flag = True
                Else
                    intent = intent - 1
                    SerialPort1.DiscardInBuffer() 'Añadido
                    Consola.Text = "Autenticación: Incorrecta"
                    Consola.Text = Consola.Text & vbCrLf & "Presione MCLR para intentarlo nuevamente"
                    Consola.Text = Consola.Text & vbCrLf & "Intentos restantes: " & intent
                    If intent = 0 Then
                        intent = 4
                        Consola.Text = "Error de protocolo:"
                        Consola.Text = Consola.Text & vbCrLf & "Cerrando puerto: " & Puertos.Text
                        SerialPort1.Close()
                        Consola.Text = Consola.Text & vbCrLf & ""
                        Consola.Text = Consola.Text & vbCrLf & "No se ha podido autenticar con JBoot"
                        Consola.Text = Consola.Text & vbCrLf & ""
                        Consola.Text = Consola.Text & vbCrLf & "Posibles soluciones:"
                        Consola.Text = Consola.Text & vbCrLf & "*Conectese nuevamente a: " & Puertos.Text
                        Consola.Text = Consola.Text & vbCrLf & "*Verifique la conexión Rx/Tx"
                        Consola.Text = Consola.Text & vbCrLf & "*Verifique su cable de comunicación"
                    End If
                End If
                End If
        Catch ex As IndexOutOfRangeException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SerialPort1_DataReceived(ByVal sender As Object, ByVal e As SerialDataReceivedEventArgs)
        'Handles SerialPort1.DataReceived
        Try
            UpdateFormDelegate1 = New UpdateFormDelegate(AddressOf PicCom)
            Dim n As Integer = SerialPort1.BytesToRead()
            If flag = False And n > 6 Then
                StrBufferIn = New Byte(n - 1) {}
                SerialPort1.Read(StrBufferIn, 0, n)
                Me.BeginInvoke(UpdateFormDelegate1)
            ElseIf flag = True And n > 3 Then
                'If n > 3 Then 'Serial Threshold evita que salte con un solo Byte
                RTIF = True
                StrBufferIn = New Byte(n - 1) {}
                SerialPort1.Read(StrBufferIn, 0, n)
                Me.BeginInvoke(UpdateFormDelegate1)
            End If
        Catch ex As TimeoutException
            SerialPort1.ReadTimeout = -1
        End Try
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timer1.Interval = timeout
        timeout = timeout - 1
        If timeout = 0 Then
            Consola.Text = "Tiempo de espera agotado!!!!"
            Consola.Text = Consola.Text & vbCrLf & ""
            Consola.Text = Consola.Text & vbCrLf & "Posibles causas: " & vbCrLf & "1.- No presiono el MCRL" & vbCrLf & "2.- Puerto de comunicación equivocado" & vbCrLf & "3.- Microcontrolador apagado" & vbCrLf & "4.- Cable de comunicacion dañado" & vbCrLf & "5.- JBoot no es el bootloader"
            Consola.Text = Consola.Text & vbCrLf & ""
            Consola.Text = Consola.Text & vbCrLf & "Posibles soluciones:"
            Consola.Text = Consola.Text & vbCrLf & "*Seleccione otro puerto de comunicación"
            Consola.Text = Consola.Text & vbCrLf & "*Energize el microcontrolador"
            Consola.Text = Consola.Text & vbCrLf & "*Vuelva a grabar el bootloader"
            Timer1.Stop()
            Try
                SerialPort1.Dispose()
            Catch ex As IOException
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
    Private Sub cerrrar_Click(sender As Object, e As EventArgs) Handles cerrrar.Click
        Me.Close()
    End Sub
    Private Sub minimiza_Click(sender As Object, e As EventArgs) Handles minimiza.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub Cabecera_MouseDown(sender As Object, e As MouseEventArgs) Handles Cabecera.MouseDown
        mouse_move = New Point(-e.X, -e.Y)
    End Sub

    Private Sub Cabecera_MouseMove(sender As Object, e As MouseEventArgs) Handles Cabecera.MouseMove
        If (e.Button = Windows.Forms.MouseButtons.Left) Then
            Dim position As Point
            position = Control.MousePosition
            position.Offset(mouse_move.X, mouse_move.Y)
            Me.Location = position
        End If
    End Sub
End Class